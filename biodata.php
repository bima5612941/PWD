<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 1</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">BimaSakti27</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Berita.html">Berita</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="Galeri.html">Galeri</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Cari Apo Kak?" aria-label="Search">
              <button class="btn btn-outline-primary" type="submit">Cari</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <img src="Image1.jpg" class="img-fluid" alt="UltraMen">
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body bg-primary">
                    <table class="table table-hover table-light">
                        <tbody>
                            <center><h2 style="font-style: oblique;">Biodata Diri</h2></center>
                            <table class="table table-hover table-light">
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>17110008</td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <Td>Bima Puja Kesuma</Td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>Pria</td>
                            </tr>
                            <tr>
                                <td>Tempat, Tanggal Lahir</td>
                                <td>:</td>
                                <td>Tebing Tinggi, 27 Februari 2004</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>Pakri</td>
                            </tr>
                            <tr>
                                <td>No HP</td>
                                <td>:</td>
                                <td>082279802510</td>
                            </tr>
                            <tr>
                                <td>Hobi</td>
                                <td>:</td>
                                <td>
                                    <ul>
                                    <li>Membaca</li>
                                    <li>Mengaji</li>
                                    <li>Bersedekah</li>
                                </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>Sekolah</td>
                                <td>:</td>
                                <td>
                                    <ol>
                                    <li>SDN 1 Tebing Tinggi</li>
                                    <li>MTs Muqimus Sunnah</li>
                                    <li>MA Muqimus Sunnah</li>
                                </ol>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>    
    </div>
    <script src="	https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min"></script>
</body>
</html>
